import { Component, OnInit } from "@angular/core";
import { FibonacciService } from "./services/fibonacci.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  responseIsActive: boolean = false;
  fiboNumber: number;
  fiboText: string;

  constructor(private fibonacciService: FibonacciService) {}

  ngOnInit() {
    this.activateResponse();
  }

  activateResponse() {
    this.fibonacciService.fibonacciNumber$.subscribe((response) => {
      if (response.fiboNumber !== null) {
        this.responseIsActive = true;
        this.fiboNumber = response.fiboNumber;
        this.validateResponse(response.isFibonacci);
        setTimeout(() => {
          this.responseIsActive = false;
        }, 7000);
      }
    });
  }

  validateResponse(isValid: boolean) {
    if (isValid) {
      this.fiboText = "Es un número Fibonacci!";
    } else {
      this.fiboText = "No es un número Fibonacci!";
    }
  }
}
