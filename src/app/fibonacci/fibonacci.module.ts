import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { FibonacciRoutingModule } from "./fibonacci-routing.module";
import { CalculationComponent } from "./calculation/calculation.component";

@NgModule({
  declarations: [CalculationComponent],
  imports: [
    CommonModule,
    FibonacciRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [CalculationComponent],
})
export class FibonacciModule {}
