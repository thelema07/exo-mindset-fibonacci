import { Component, OnDestroy, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { Subscription } from "rxjs";
import { FibonacciService } from "src/app/services/fibonacci.service";

@Component({
  selector: "calculation",
  templateUrl: "./calculation.component.html",
  styleUrls: ["./calculation.component.scss"],
})
export class CalculationComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription = new Subscription();
  private fibonacciForm: FormGroup;

  constructor(
    private form: FormBuilder,
    private fibonacciService: FibonacciService
  ) {}

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.fibonacciForm = this.form.group({
      fiboNumber: new FormControl(null, Validators.required),
    });
  }

  private validateFibonacciNumber() {
    const { fiboNumber } = this.fibonacciForm.value;
    const validateFibonacci = this.fibonacciService
      .validateFibonacci(fiboNumber)
      .subscribe((response) => {});
    this.subscriptions.add(validateFibonacci);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
