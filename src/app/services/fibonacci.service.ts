import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, of, Subject } from "rxjs";
import { Fibonacci } from "../interfaces/fibonacci";

@Injectable({
  providedIn: "root",
})
export class FibonacciService {
  private _fibonacciNumber = new BehaviorSubject<Fibonacci>({
    fiboNumber: null,
    isFibonacci: false,
  });
  public fibonacciNumber$: Observable<
    Fibonacci
  > = this._fibonacciNumber.asObservable();

  constructor() {}

  isFibonacci(fibo: number, series: number) {
    let seriesArray = [0, 1];

    for (let i = 2; i < series; i++) {
      seriesArray.push(seriesArray[i - 2] + seriesArray[i - 1]);
    }

    return seriesArray.includes(fibo);
  }

  validateFibonacci(fibo: number): Observable<Fibonacci> {
    let result = {
      fiboNumber: fibo,
      isFibonacci: false,
    };
    result.isFibonacci = this.isFibonacci(fibo, 30);

    this._fibonacciNumber.next(result);

    return of(this._fibonacciNumber.value);
  }
}
